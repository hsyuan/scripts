set nu
syntax on
colorscheme desert
filetype plugin indent on
set mouse=a
set tags=tags
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set showmatch
set tags=tags;
set autochdir
set showcmd
set ruler

