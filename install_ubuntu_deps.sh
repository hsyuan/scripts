sudo apt update
sudo apt install vim gedit
 
sudo apt-get install -y libprotobuf-dev libleveldb-dev libsnappy-dev libopencv-dev libhdf5-serial-dev protobuf-compiler
 
sudo apt-get install -y --no-install-recommends libboost-all-dev
 
sudo apt-get install -y libopenblas-dev liblapack-dev libatlas-base-dev
 
sudo apt-get install -y libgflags-dev libgoogle-glog-dev liblmdb-dev
 
sudo apt-get install -y git cmake build-essential
