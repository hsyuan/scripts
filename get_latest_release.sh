#! /bin/bash
#navigate to script path
GIT_SERVER=172.28.141.75
#DIRNAME=$(cd $(dirname $0);pwd)
#cd $DIRNAME

NAME=$(curl ftp://AI:dailybuild@${GIT_SERVER}/all_in_one/release_build/ | sort | tail -n 1 | awk '{print $11}')
echo "the latest bmsdk release time is " ${NAME}
touch last_version
echo "sdk_dir = $NAME"

last_tim=`cat last_version`
if [[ "${last_time}" != ${NAME} ]];then
   echo "download the latest bmsdk version"
   TAR_FILE=$(curl ftp://AI:dailybuild@${GIT_SERVER}/all_in_one/release_build/${NAME}/bmnnsdk2/ |awk '{print $NF}')
   echo "TAR_FILE=$TAR_FILE"
   sdk_ver=${TAR_FILE:17:5}
   echo "sdk_ver=$sdk_ver"
   if [ ! -d "$sdk_ver" ];then
       mkdir $sdk_ver
   fi
   echo "sdk_ver=$sdk_ver"
   if [ ! -n "$sdk_ver" ];then
	   echo "error: sdk_ver=$sdk_ver"
	   exit 1
   fi
   pushd $sdk_ver
   rm -fr bmnnsdk2-*
   echo "TAR_FILE=$TAR_FILE"
   TAR_DIR_NAME="${TAR_FILE/.tar.gz}"
   url=ftp://AI:dailybuild@${GIT_SERVER}/all_in_one/release_build/${NAME}/bmnnsdk2/${TAR_FILE}
   echo "url=$url"
   wget ${url}
   wget ftp://AI:dailybuild@${GIT_SERVER}/all_in_one/release_build/${NAME}/bmnnsdk2/bmnnsdk2.MD5
   wget ftp://AI:dailybuild@${GIT_SERVER}/all_in_one/release_build/${NAME}/bmnnsdk2/release_version.txt
   echo  "TAR_DIR_NAME=$TAR_DIR_NAME"
   #tar zxf $TAR_FILE
   popd
else
   echo "already the latest"
#   cd release/test && source envsetup_pcie.sh
fi
echo $NAME >last_version
