#!/bin/bash
CONDA_INSTALL_DIR=/mnt/d/miniconda3
USAGE="Usage:install_miniconda.sh -t /path/to/install"
argc=$#

if [[ $argc -eq 0 ]];then
    echo $USAGE
    exit 1
fi

while getopts ":t:" opt; do
  case $opt in
    t) CONDA_INSTALL_DIR=$OPTARG ;;
    ?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

echo "CONDA_INSTALL_DIR  = ${CONDA_INSTALL_DIR}"
mkdir -p $CONDA_INSTALL_DIR
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O $CONDA_INSTALL_DIR/miniconda.sh
bash $CONDA_INSTALL_DIR/miniconda.sh -b -u -p $CONDA_INSTALL_DIR
rm $CONDA_INSTALL_DIR/miniconda.sh
