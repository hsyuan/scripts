# 安装docker的快捷脚本：1
# 1、先检查docker是否已经本地安装，如果已安装就跳过，否则执行docker安装指令
# 2、将当前用户加入docker组，这样每次执行docker就不需要加sudo了。

if [ ! -f /usr/bin/docker ];then
	echo "install docker.io"
	sudo apt install docker.io
fi
echo "add current user to docker group"
sudo gpasswd -a ${USER} docker
sudo service docker restart
newgrp docker


