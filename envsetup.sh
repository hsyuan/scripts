#terminal color for mac

export CLICOLOR='Yes'
export LSCOLORS='ExGxFxdaCxDaDahbadacec'

OS=MacOS
if [ -f '/etc/issue' ];then
      OS=Linux
fi

alias rgrep="grep --binary-files=without-match -nHR "
alias ll='ls -lh'
alias indent="indent -kr -ts4 -nut -i4 -cdw -bli 0 -sob -l120"

export GIT_EDITOR=vim
#export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

export BMWS=$HOME/bitmain/work
#export GIT_URL=ssh://huaishan.yuan@10.14.7.3:29418
export GIT_URL=ssh://huaishan.yuan@gerrit-ai.sophgo.vip:29418

if [[ $OS == 'MacOS' ]];then
    eval "$(pyenv init -)"
fi

# set cross compiler
if [[ $OS == 'Linux' ]];then
    PS1='\[\e[0;33m\]\u@\h\[\e[0m\]:\[\e[0;35m\]\W\[\e[0m\]\$ '
    
    export PATH=$PATH:$BMWS/bm_prebuilt_toolchains/loongarch64-linux-gnu-2021-04-22-vector/bin
    export PATH=$PATH:$BMWS/bm_prebuilt_toolchains/x86-64-core-i7--glibc--stable/bin
    export PATH=$PATH:/usr/sw/swgcc830_cross_tools-bb623bc9a/usr/bin
    export PATH=$PATH:$BMWS/bm_prebuilt_toolchains/mips-loongson-gcc7.3-linux-gnu/2019.06-29/bin
    export PATH=$PATH:$BMWS/bm_prebuilt_toolchains/gcc-linaro-6.3.1-2017.05-x86_64_aarch64-linux-gnu/bin
    export REL_TOP=$HOME/bmnnsdk2/bmnnsdk2-latest

    function set_bmnnsdk2() {
        #export LD_LIBRARY_PATH=$REL_TOP/bmnn/lib/pcie:$REL_TOP/lib/ffmpeg/x86:$REL_TOP/lib/decode/x86:$REL_TOP/lib/opencv/x86
        echo "REL_TOP=$REL_TOP"
        pushd $REL_TOP/scripts
        source envsetup_pcie.sh
        popd
    }

fi

function gpush() {
    branch=master
    if [ -n "$1" ];then
	branch=$1
    fi
    git push origin HEAD:refs/for/$branch
}

function clone_all() {
	if [ ! -d "$BMWS" ]; then
		echo "ERROR: env BMWS=${BMWS} not exist!"
		return 1
	fi
	pushd $BMWS
        modules="bm_prebuilt_toolchains sophon-inference bmiva4"
        for mod in ${modules[@]}/
        do
           if [ -d "${mod}" ];then
		      cd ${mod}
              git pull
              cd ..
	       else
		      git clone $GIT_URL/${mod}.git
	       fi 
        done
	popd
}

function ffmpeg_push_rtsp() {

    if [ ! -n "$1" ]; then
        FILE=~/testfiles/yanxi-1080p.264
    else
        FILE=$1
    fi
   
    if [ ! -n "$2" ]; then
        URL=rtsp://127.0.0.1:8554/test
    else
        URL=$2
    fi
    while true
    do 
       ffmpeg -re -i "$FILE" -c copy -f rtsp -y $URL; \
    done
}

function docker_init() {
	sudo gpasswd -a ${USER} docker
	sudo service docker restart
	newgrp docker
}

